"""
autograder 
"""
import csv
import json
import traceback
import statistics as stats
from string import Template
import matplotlib.pyplot as plt
import os
import sys

config  = None
people = None
assignments = None

def main():
    # open and load the configuration file
    config = loadConfig()

    # parse args

    gradesFile = 'grades.csv' if len(sys.argv) == 1 else sys.argv[1]

    scores = loadScores(config, gradesFile)


    people = createPeople(config, scores)
    
    template = ((loadTemplates())['HTMLStudent'])

    assignments = makeAssignments(config, people) 
    createSummaryGraphs(config, [1,1,1,2,3,4,5,6,6,6,6])
    writeSummary(config, people, assignments)

    print("Summaries written to Summaries/")

def writeSummary (config, people, assignments):
    """
    Write the course to html
    """
    templates = loadTemplates()
    studentHrefs = writeSummaryStudents(config, people, templates)
    assignRefs = writeSummaryAssignments(config, assignments, templates)

def writeSummaryAssignments (config, assignments, templates):
    """
    Write summary of the assignments to HTML 
    """
    for cat, asi in assignments.items():
        dirName = 'Summaries/Assignments/{}'.format(\
            cat.replace(", ","").replace(" ",""),
            )
        if not os.path.exists(dirName):
            os.makedirs(dirName)

        for name, single in asi.items():
            template = (templates['HTMLAssignment'])

            fileName = dirName + '/{}.html'.format(\
                name.replace(", ","").replace(" ",""),
                )
                
            scoreSummary = single.summarize(verbose=True).replace("\n","<br>")

            scores = []
            for i in single.scores:
                scores.append(i['score'])
            
            plots = createSummaryGraphs(config, scores, single.name, fileDir=cat)
            rules = getRules(config, typeName=cat)
            
            with open(fileName, 'w') as outfile:
                print(template.substitute(Assignment=name, Category=cat,
                    worth=rules['weight'], 
                    number=len(scores),
                    drops=rules['numberToDrop'],
                    cdf="../../../" + plots['cdf'], 
                    scores=scoreSummary), 
                    file=outfile)

def writeSummaryStudents (config, people, templates):
    studentRefs = ""
    for name , person in people.items():
        template = (templates['HTMLStudent'])
        fileName = 'Summaries/Students/{}.html'.format(\
            name.replace(", ","").replace(" ",""))

        href = "<li><a href=\"Students/{}\">{}</a></li>".format(name.replace(",","").replace(" ",""),\
            name)
        studentRefs += (href)

        assignmentHrefs = "" 
        assignRef = "<li><a href=\"../Assignments/{}/{}.html\">{}</a>: {}</li>"
        assignType = "<H4>{} : Worth {}%</H4>"

        for cat, grades in person.getGradesByType().items():
            shortCatName = cat.replace(" ","")
            rules = getRules(config, typeName=cat)
            assignmentHrefs += assignType.format(cat, rules['weight'])
            assignmentHrefs += ("<ul>")
            for grade in grades:
                assignmentName = grade.assignment['name']
                shortName = assignmentName.replace(' ', '')
                points = grade.assignment['points']
                score = grade.score
                scorefmt = "{} / {}".format(score, points)
                assignmentHrefs += assignRef.format(shortCatName, shortName,
                    assignmentName, scorefmt
                )

            assignmentHrefs += ("</ul>")
        
        # Write Students
        if not os.path.exists("Summaries/Students"):
            os.makedirs("Summaries/Students")

        with open(fileName, 'w') as studentFile:
            finalGrade = person.getFinalGrade()
            print(template.substitute(Name=name, \
                    letter= finalGrade[1], average=finalGrade[0],
                    assignments=assignmentHrefs
                    ),
                file=studentFile)
            
    return studentRefs

def createSummaryGraphs (config, scores, title="Assignment", fileDir="Test"):
    """
    Given list of scores and a config file, make a summary 
    """
    plt.clf()
    cutoffs = list(config['Cutoffs'].items())
    scores = sorted(scores)
    x = []
    y = []
    x.append(0)
    y.append(0)
    plots = {}

    for i in range(len(scores)): 
        y .append( (float(i)  + 1)/ len(scores)* 100) 
        x .append( scores[i])

    plt.xlabel("Grades")
    plt.ylabel("Percentage of Students")
    plt.title("CDF of Grades for {}".format(title))
    plt.axis([min(x),max(x),0,100])
    plt.plot(x,y)
    if not os.path.exists("Summaries/Plots/{}/{}/".format(fileDir, title)):
        os.makedirs("Summaries/Plots/{}/{}/".format(fileDir, title))
    plt.savefig("Summaries/Plots/{}/{}/cdf.png".format(fileDir,title))
    cdfPath = "Summaries/Plots/{}/{}/cdf.png".format(fileDir,title)
    plots['cdf'] = (cdfPath)

    return plots

    

def loadTemplates ():
    """
    loadTemplates -> dict{string : template}
    Templates are : {
        'HTMLStudent': "", $Name, letter, average, assignments
        'HTMLCourse':"", $
        'HTMLAssignment': "", $Assignment, Category, worth, number, drops, scores, cdf
    }
    """
    templates = {}
    with open('HTMLStudent.html', 'r') as myfile:
        data=myfile.read().replace('\n', '')
        templates['HTMLStudent'] = Template(data)
    with open('HTMLAssignment.html', 'r') as myfile:
        data=myfile.read().replace('\n', '')
        templates['HTMLAssignment'] = Template(data)
    with open('HTMLCourse.html', 'r') as myfile:
        data=myfile.read().replace('\n', '')
        templates['HTMLCourse'] = Template(data)
    return templates    

def summarizeFinalGrades(config, people):
    string = ""
    cutoffs = list(config['Cutoffs'].items())
    finalGrades = getFinalGrades(config, people)
    gradesAsNumbers = list(map(lambda x : x[0], finalGrades))
    finalGrades.sort(key=lambda el : el[0])
    mini = int(min(map(lambda el : el[0], finalGrades)))
    maxi = int(max(map(lambda el : el[0], finalGrades)))
    mean, median, dev= \
        stats.mean(gradesAsNumbers),\
        stats.median(gradesAsNumbers),\
        stats.stdev(gradesAsNumbers)\
    
    string += "Total Grades:\n"
    string += "Mean:   {}\n".format(mean)
    string += "Median: {}\n".format(median)
    string += "stdev:  {}\n".format(dev)
    string += "Min:    {}\n".format(mini)
    string += "Max:    {}\n".format(maxi)
    string += "------------------------------\n"
    
    for i in range (100, mini-1, -1):
        string += "{:>3}: ".format(i)

        for finalGrade in finalGrades: 
            if(abs(finalGrade[0] - i) < 1):
                string += "X"
        for cutoff in cutoffs:
            if ( i == cutoff[1]):
                string += ("\n---------------------- {}".format(cutoff[0]))
        string += "\n"
                

    return string

    
def getFinalGrades(config, people):
    finalGrades = []
    for name, person in people.items():
        val, letter = person.getFinalGrade()
        finalGrades.append((val,letter))
    return finalGrades



def getRules (config, typeName : str):
    for arule in config['Types']:
        if (arule['TypeName'] == typeName):
            return arule

def makeAssignments(config, people):
    """
    create a list of assignments by type then by name
    """
    types = {}
    for assignment in config['Assignments']:
        if(not assignment['TypeName'] in types):
            types[assignment['TypeName']] = {}
        name = assignment['name']
        typename = assignment['TypeName'] 
        points = assignment['points']
        
        scores = []
        # for each student, add their scores to the assignment
        for _, student in people.items():
            for grade in student.grades: 
                if(name == grade.assignment['name']):
                    scores.append({"score": grade.score, "student": student.name})

        types[typename][name] = Assignment(name, points, scores, typename, config)

    return types 
    

    
def loadConfig():
    "get the configuration object from config.json"
    with open("config.json") as configFile:
        # ensure that assignment types add up to 100
        config =  json.load(configFile)
        types = config['Types']
        assignmentScoreSum = 0
        for assignmentType in types:
            assignmentScoreSum += float(assignmentType['weight'])
            
        if(not assignmentScoreSum == 100):
            print("Warning: the sum of all the assignments does not equal 100. Please fix this")
            print(traceback.extract_stack())
            exit(1)

        

        return config


def loadScores(config, inputFile='grades.csv'):
    "return the scores as a list of people"
    rows = []
    print(inputFile)
    if(not os.path.isfile(inputFile)):
        print("Not a file: " , inputFile)
        exit(1)
    
    with open(inputFile, 'r') as csvfile:
        reader = csv.DictReader(csvfile, delimiter='\t')
        for row in reader:
            rows.append(row)
    return rows  


def createPeople (config, grades):
    """
    turn people into objects
    """
    types = config['Types']
    assignments = config["Assignments"]

    people = {}

    for person in grades:
        clean_person = {}
        clean_person['id'] = person['LastName, FirstName']
        clean_person['grades'] = []

        for assignment in assignments:
            score = int(person[assignment['name']])
            
            grade = Grade(assignment, score)
            clean_person['grades'].append(grade)
            

        people[clean_person['id']] = Student(name=clean_person['id'], \
        grades=clean_person['grades'], config=config)
        
    return people

class Student:
    def __init__(self, name: str, grades: list, config):
        """
        grades: [ {score: num , assignment : {TypeName: str, points: num, name: str}} ]
        """
        self.name = name
        self.grades = grades
        self.config = config
    
    def __str__(self):
        string = "{name: " + self.name + ", grades: " 
        string += ",".join(map (lambda grade : str(grade), self.grades))
        string += "}"
        return string
 
    def getGradesByType(self) :
        """
        return maps of type name to grade
        """
        types = {}
       
        for grade in self.grades:
            if (not grade.assignment['TypeName'] in types) :
                types[grade.assignment['TypeName']] = []
            types[grade.assignment['TypeName']].append(grade)

        return types
                
    def getGradesWithDrops(self) :
        """
        get grades by types, remove the drops
        """
        
        gradesByType = self.getGradesByType()
        cleanedGrades = {}
        for grade_type , grades in gradesByType.items():
            grades.sort(key= lambda grade : grade.score ,reverse=True)
            typeName = grades[0].assignment['TypeName']
            rules = getRules(self.config, typeName)
            numberToDrop = int(rules['numberToDrop']) 
            grades = grades[0: len(grades) - numberToDrop]
            cleanedGrades[grade_type] = grades

        return cleanedGrades

    # get average by type
    def getAverageByType(self):
        """
        get the quiz average etc. Assuming each assignment in the group is identical 
        """
        gradesByType = self.getGradesWithDrops()
        avgByType = {}

        for gradeCategory, grades in gradesByType.items():
            total = 0
            length = len(grades)
            for grade in grades: 
                points = grade.assignment['points']
                score = grade.score
                percent =  score / float(points) 
                total += percent
            if(length == 0):
                total = 1
            else: 
                total  /= length
                
            avgByType[gradeCategory] = total

        return avgByType
            
    
    def getFinalGrade(self):
        """
        Return final grade number and final grade letter
        """
        avgByType = self.getAverageByType()

        letter = 'NR'
        score = 0
        # get the weight of each assignment, 
        # multiple the weight * assignment average * 100
        for assign_type, value in avgByType.items():
            rules = getRules(self.config, assign_type)
            weight = float(rules['weight'])
            weighted_score = weight * value
            score += weighted_score

        # get letter grades
        cutoffs = list(self.config['Cutoffs'].items())
        cutoffs.sort(key=lambda x : x[1], reverse=True)

        for cutoff in cutoffs:
            if (score >= cutoff[1]):
                letter = cutoff[0]
                break;

        return score, letter
        
        
    def getReportAsText(self):
        string = ""
        string += (self.name)  + "   "
        grade, score = self.getFinalGrade()
        string += "{}, {}\n".format(grade, score)
        string += "-----------------------\n"
        gradesByType = self.getGradesByType()
        for gradeType, grades in gradesByType.items():
            string += gradeType + "\n"

            for grade in grades:
                string += "\t"

                string += "{0}: {1}/{2}".format(\
                    grade.assignment['name'], grade.score,\
                    grade.assignment['points'])
                # TODO
                # string += grade.assignment.summarize()

                string += "\n"

                


        return string

        # want


        
    # get average

        
 
    
class Grade:
    def __init__(self, assignment: dict, score: int ):
        self.assignment = assignment
        self.score = score

    def __str__(self):
        return "{ score: " + str(self.score) +  ", " + "assignment: " + \
        str(self.assignment) + "}"
    

class Assignment:
    def __init__(self, name: str, points: int, scores: list, assignment_type: str, config: dict):
        self.name = name
        self.points = points
        self.scores = scores
        self.assignment_type = assignment_type; 
        self.config = config

    def getRange(self):
        return min(map(lambda x : x['score'],self.scores)), \
            max(map(lambda x : x['score'], self.scores))
    def getMean(self):
        return stats.mean(map(lambda x : x['score'],self.scores))

    def getMedian(self):
        """
        get median, low_median, high_median
        """
        return stats.median(map(lambda x : x['score'],self.scores))

    def getDev(self): 
        """
        get stdev
        """
        return stats.stdev(map(lambda x : x['score'], self.scores))
    
    def summarize(self, verbose=False):
        median, mean, dev= \
            self.getMedian(), self.getMean(), self.getDev()
        scoremin, scoremax = self.getRange()
        string = "Assignment: {}\n".format(self.name) 
        string += "median:  {}\nmean:    {}\nstddev:  {}\nmax:     {}\nmin:     {}\n".\
            format(median, mean, dev, scoremax, scoremin)

        # get list of grade cutoffs
        cutoffs = list(self.config['Cutoffs'].items())
        localCutoffs = lambda x : int(x / 100 * scoremax)
        
        if(not verbose):
            return string
        
        string += "------------------\n"
        scoreMin, scoreMax = self.getRange()
        for i in range(scoreMax, scoreMin-1, -1):
            string += "{:>3}: ".format(i)
            for score in self.scores: 
                scoreNumber = score['score']
                if(scoreNumber is i ):
                    string += "X" 
            for cutoff in cutoffs:
                if ( i == localCutoffs(cutoff[1])):
                    string += ("\n---------------------- {}".format(cutoff[0]))
            
            string += "\n"

                
        return string
        


    def __str__(self):
        return "{ name: "  + self.name + ", scores: " + str(self.scores) + "}"
    

if __name__ == '__main__':
    main()