# Autograder

Converts csv file of student grades + a config file to html pages. Program loads data from grades.csv (or first command line arg), and creates a directory 'Summaries'. The structure is given below. 

```
Summaries/
├── Assignments
│   ├── Quiz
│   │   ├── quiz1.html
│   └── Test
│       └── test1.html
├── Plots
│   ├── Quiz
│   │   ├── quiz1
│   │   │   └── cdf.png
│   └── Test
│       ├── Assignment
│       │   └── cdf.png
└── Students
    └── raeJohn.html
```

## Setup

```
pip install -r requirements.txt
```

*Note*: matplotlib requires python3-tk from the ubuntu packages. This can be done with `sudo apt install python3-tk`


## Running the Program

- Linux: 

        python3 Autograder.py

- Windows: 

        python Autograder.py

*Note*: Requires python 3.5 for some type declarations


## Input: 
- config.json

    This file contains the definition for each assignment & their weights. This *must* contain a definition for each assignment in the csv file. 

  ``` json
  {
    "CourseName":"Test",
    // Each assignment type defines what % of final grade it is & number to drop
    "Types" : [
        {
            "TypeName": "Quiz", 
            "weight":"30", // as a percent
            "numberToDrop":"0"
        }
    ],
   "Assignments" : [
       // Each assignment defines type, points, and name
       {
        "name": "Quiz 1",
        "points":"10", // does not affect weight; each quiz is equal
        "TypeName": "Quiz"
       }
   ],
    "Cutoffs": {
        "S": 95,
        "A": 87,
        "B": 78,
        "C": 70,
        "D / NR": 60
    }
  }
   
  ```

-  grades.csv [or first argument to program]

    *Note*: tabs separate elements

    ``` tsv
    LastName, FirstName	username	section	quiz1	quiz2	test1
    Doe, John 	jdoe 	1	10	20	100
    ```

## TODO

- Make a index / listing file

- make more robust config file

- Toggle grade cutoffs